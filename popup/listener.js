const click_play_period_apple = 30;
const scatter_apple = 1;
const click_play_period_spotify = 30;
const scatter_spotify = 1;
const click_next_apple = 30;
const scatter_next_apple = 30;
const click_next_spotify = 30;
const scatter_next_spotify = 30;
const refresh = 1;
const scatter_refresh = 1;

(function () {

    window.click_play_period_apple =  click_play_period_apple ;
    window.scatter_apple =  scatter_apple ;
    window.click_play_period_spotify =  click_play_period_spotify;
    window.scatter_spotify =  scatter_spotify;

    window.click_next_apple =  click_next_apple;
    window.scatter_next_apple =  scatter_next_apple;

    window.click_next_spotify =  click_next_spotify;
    window.scatter_next_spotify =  scatter_next_spotify;

    /**---------------------------- start copy point -------------------------------------*/
    window.allowReload = false;

    var randomInterval = function (interval, random) {
        let rand = Math.floor(Math.random() * random);
        return parseInt(interval) + rand;
    }

    try {

        var infinitePlayApple = function () {

            const controlButtons = document.getElementsByClassName('web-chrome-playback-controls__directionals')[0] || false;
            let disabled = function (button) {
                return button && button.classList.contains('active') === false;
            }
            if (controlButtons) {
                const shuffle = controlButtons.children[0];
                const repeat = controlButtons.children[2];
                if (disabled(shuffle)) {
                    shuffle.click();
                }
                if (disabled(repeat)) {
                    repeat.click();
                }
            }

            const player = document.getElementsByClassName('web-chrome-playback-controls__main')[0] || false;
            let playerButton = false;
            try {
                playerButton = player.children[1];
                if (playerButton.getAttribute('aria-label') === "Play") {
                    playerButton.click();
                }
                if (playerButton.getAttribute('disabled') === '') {
                    const allPlayButtonsLst = document.getElementsByClassName('play-button');
                    const listLength = allPlayButtonsLst.length;
                    const randomPlayButton = allPlayButtonsLst[Math.floor(Math.random() * listLength)];
                    randomPlayButton.click();
                }
            } catch (e) {
                console.log("Apple music is not ready: " + e.message);
            }
            let rand = randomInterval(click_play_period_apple, scatter_apple);
            console.log("Apple music interval: " + rand + " seconds.");
            setTimeout(infinitePlayApple, rand * 1000);
        }
        var nextApple = function () {
            const playerButtons = document.getElementsByClassName('web-chrome-playback-controls__main')[0] || false;
            if (playerButtons) {
                const next = playerButtons.children[2];
                if (next) {
                    next.click();
                }
            }
            let rand = randomInterval(click_next_apple, scatter_next_apple);
            console.log("Apple music next: " + rand + " seconds.");
            setTimeout(nextApple, rand * 1000);
        }

        var infinitePlaySpotify = function () {
            let playButtons = [];
            for (let item of document.getElementsByTagName('button')) {
                if ("Play" === item.title) {
                    playButtons.push(item);
                }
                if ("Enable shuffle" === item.title) {
                    item.click();
                }
                if ("Enable repeat" === item.title) {
                    item.click();
                }
            }
            setTimeout(function(){
                let item = playButtons[Math.floor(Math.random() * playButtons.length)];
                if (item !== undefined) {
                    item.click();
                    console.log(item);
                } else {
                    console.log(playButtons);
                }
            }, 500);
            let rand = randomInterval(click_play_period_spotify, scatter_spotify);
            console.log("Spotify interval: " + rand + " seconds.");
            setTimeout(infinitePlaySpotify, rand * 1000);
        }
        var nextSpotify = function () {
            let skipButton = null;
            for (let item of document.getElementsByTagName('button')) {
                if ("Next" === item.title){
                    skipButton = item;
                }
            }
            if (skipButton !== null) {
                skipButton.click();
            }
            let rand = randomInterval(click_next_spotify, scatter_next_spotify);
            console.log("Spotify next: " + rand + " seconds.");
            setTimeout(nextSpotify, rand * 1000);
        }

        var refreshPage = function () {
            if (allowReload) {
                preventBeforeReloadPopUp();
                location.reload();
            }
            window.allowReload = true;
            let rand = randomInterval(refresh, scatter_refresh);
            setTimeout(refreshPage, rand * 60);
            rand = Math.round(rand / 1000);
            console.log("Reload rand: " + rand + " minutes");
        }

        var preventBeforeReloadPopUp = function () {
            window.onbeforeunload = function(event) {
                event.preventDefault();
                event.returnValue = '';
                return '';
            }
        }

        var removeAdModal = function () {
            let modal = document.getElementById('dt-modal-container');
            if (modal) {
                modal.remove();
            }
        }

        if (location.href.indexOf('apple.com') !== -1) {
            infinitePlayApple();
            nextApple();
        }

        if (location.href.indexOf('spotify.com') !== -1) {
            infinitePlaySpotify();
            nextSpotify();
        }

        setInterval(function () {
            preventBeforeReloadPopUp();
            removeAdModal();
        }, 1000);

        refreshPage();
    } catch (e) {
        console.log(e);
    }
    /**---------------------------- end copy point -------------------------------------*/
})();

#!/usr/bin/env sh

zip -r infinite.xpi . -x .git/\* -x .idea/\* -x .gitignore -x "pack.sh" -x .DS_Store -x popup/.DS_Store